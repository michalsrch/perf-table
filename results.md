# Results

Each value is an average of 3 test results.


## Environment

* MacBook Pro 13-inch, 2017
* 2.3 GHz Intel Core i5
* 8 GB 2133 MHz LPDDR3
* Intel Iris Plus Graphics 640 1536 MB
* macOS Sierra 10.12.6
* Google Chrome 70.0.3538.16 (Official Build) beta (64-bit)


## Measurements

### Initial page load, 100 rows

* Started at 28ms
* DomContentLoaded event at 275ms
* Load event at 315ms
* Scripting: 61.8ms
* Rendering: 19.0ms
* Painting: 4.1ms
* Memory heap snapshot: 7.9 MB


### 100 rows to 200 rows

* Scripting: 23.7ms
* Rendering: 19.8ms
* Painting: 2.6ms
* Memory heap snapshot: 8.3 MB


### 200 rows to 500 rows

* Scripting: 37.6ms
* Rendering: 53.8ms
* Painting: 1.7ms
* Memory heap snapshot: 9.8 MB


### 500 rows to 1000 rows

* Scripting: 37.0ms
* Rendering: 109.7ms
* Painting: 3.1ms
* Memory heap snapshot: 12.4 MB


### 100 rows to 500 rows

* Scripting: 59.6ms
* Rendering: 69.6ms
* Painting: 2.4ms
* Memory heap snapshot: 10.1 MB


### 100 rows to 1000 rows

* Scripting: 86.2ms
* Rendering: 165.2ms
* Painting: 3.6ms
* Memory heap snapshot: 12.3 MB


### hover at 100 rows

* Scripting: 0.1ms
* Rendering: 0.1ms
* Painting: 0.1ms
* Memory heap snapshot: 7.8 MB


### hover at 200 rows

* Scripting: 0.1ms
* Rendering: 0.3ms
* Painting: 0.7ms
* Memory heap snapshot: 8.5 MB


### hover at 500 rows

* Scripting: 0.1ms
* Rendering: 0.3ms
* Painting: 0.7ms
* Memory heap snapshot: 10.2 MB


### hover at 1000 rows

* Scripting: 0.1ms
* Rendering: 0.3ms
* Painting: 0.7ms
* Memory heap snapshot: 12.4 MB


### responsive (reducing window size) change at 100 rows

* Scripting: 0.0ms
* Rendering: 16.8ms
* Painting: 5.6ms
* Memory heap snapshot: 8.0 MB


### responsive (reducing window size) change at 200 rows

* Scripting: 0.0ms
* Rendering: 29.7ms
* Painting: 6.2ms
* Memory heap snapshot: 8.6 MB


### responsive (reducing window size) change at 500 rows

* Scripting: 0.0ms
* Rendering: 71.5ms
* Painting: 6.2ms
* Memory heap snapshot: 10.2 MB


### responsive (reducing window size) change at 1000 rows

* Scripting: 0.0ms
* Rendering: 137.9ms
* Painting: 6.2ms
* Memory heap snapshot: 12.5 MB


### column style change change (from first to third) at 100 rows

* Scripting: 1.1ms
* Rendering: 7.5ms
* Painting: 5.1ms
* Memory heap snapshot: 8.0 MB


### column style change change (from first to third) at 200 rows

* Scripting: 1.2ms
* Rendering: 12.9ms
* Painting: 5.2ms
* Memory heap snapshot: 8.8 MB


### column style change change (from first to third) at 500 rows

* Scripting: 1.2ms
* Rendering: 30.7ms
* Painting: 5.2ms
* Memory heap snapshot: 10.3 MB


### column style change change (from first to third) at 1000 rows

* Scripting: 1.3ms
* Rendering: 68.4ms
* Painting: 5.4ms
* Memory heap snapshot: 12.4 MB


### theme change (from default to custom) at 100 rows

* Scripting: 2.2ms
* Rendering: 22.2ms
* Painting: 4.6ms
* Memory heap snapshot: 7.9 MB


### theme change (from default to custom) at 200 rows

* Scripting: 2.2ms
* Rendering: 40.5ms
* Painting: 4.6ms
* Memory heap snapshot: 7.9 MB


### theme change (from default to custom) at 500 rows

* Scripting: 4.2ms
* Rendering: 96.2ms
* Painting: 4.8ms
* Memory heap snapshot: 10.3 MB


### theme change (from default to custom) at 1000 rows

* Scripting: 6.3ms
* Rendering: 195.7ms
* Painting: 6.2ms
* Memory heap snapshot: 12.9 MB
