import React from 'react'
import PropTypes from 'prop-types'
import { withStateHandlers } from 'recompose'

import Table from '../Table'
import { words } from './data'
import get from 'lodash/get'

import classes from './App.css'
import themedTable from './ThemedTable.less'

const defaultTheme = {}

const customTheme = {
  Table: themedTable,
}

const registeredThemes = {
  default: defaultTheme,
  custom: customTheme,
}

export const ThemeContext = React.createContext({})

const withAppState = withStateHandlers(
  { limit: 100, highlight: 0, activeTheme: 'default', theme: defaultTheme },
  {
    onLimitChange: () => (limit) => ({ limit }),
    onHighlightChange: () => (highlight) => ({ highlight }),
    onThemeChange: ({ activeTheme }) => (newTheme) =>
      activeTheme !== newTheme
        ? {
            activeTheme: newTheme,
            theme: get(registeredThemes, [newTheme], defaultTheme),
          }
        : null,
  },
)

const App = ({
  activeTheme,
  highlight,
  limit,
  onHighlightChange,
  onLimitChange,
  onThemeChange,
  theme,
}) => (
  <div className={classes.container}>
    <div className={classes.header}>
      Number of items:{' '}
      <select
        value={String(limit)}
        onChange={(event) => onLimitChange(+event.target.value)}
      >
        <option value="10">10</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="200">200</option>
        <option value="500">500</option>
        <option value="1000">1000</option>
      </select>{' '}
      Highlight value col:{' '}
      <select
        value={String(highlight)}
        onChange={(event) => onHighlightChange(+event.target.value)}
      >
        <option value="0">none</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
      </select>{' '}
      Active theme:{' '}
      <select
        value={activeTheme}
        onChange={(event) => onThemeChange(event.target.value)}
      >
        <option value="default">default</option>
        <option value="custom">custom</option>
      </select>
    </div>
    <ThemeContext.Provider value={theme}>
      <Table data={words} limit={limit} highlight={highlight} />
    </ThemeContext.Provider>
  </div>
)

App.propTypes = {
  activeTheme: PropTypes.string.isRequired,
  highlight: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
  onHighlightChange: PropTypes.func.isRequired,
  onLimitChange: PropTypes.func.isRequired,
  onThemeChange: PropTypes.func.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withAppState(App)
