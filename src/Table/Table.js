import React from 'react'
import PropTypes from 'prop-types'

import map from 'lodash/map'
import take from 'lodash/take'

class TableHead extends React.PureComponent {
  render() {
    return (
      <thead>
        <tr>
          <th>Word</th>
          <th>Words</th>
          <th>Value 1</th>
          <th>Value 2</th>
          <th>Value 3</th>
          <th>Value 4</th>
          <th>Value 5</th>
          <th>Value 6</th>
          <th>Value 7</th>
          <th>Tools</th>
        </tr>
      </thead>
    )
  }
}

class TableRow extends React.PureComponent {
  static propTypes = {
    item: PropTypes.string.isRequired,
  }

  render() {
    const { item } = this.props
    const num = item.length
    return (
      <tr>
        <td>{item}</td>
        <td>{`${item} ${item} ${item} ${item} ${item} ${item} ${item}`}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>{num}</td>
        <td>edit</td>
      </tr>
    )
  }
}

class TableBody extends React.PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.string).isRequired,
    limit: PropTypes.number.isRequired,
  }

  render() {
    const { data, limit } = this.props
    return (
      <tbody>
        {map(take(data, limit), (item) => (
          <TableRow key={item} item={item} />
        ))}
      </tbody>
    )
  }
}

const Table = ({ data, limit, highlight, classes }) => (
  <table className={classes.table}>
    <colgroup>
      <col />
      <col />
      <col className={highlight === 1 ? classes.highlighted : undefined} />
      <col className={highlight === 2 ? classes.highlighted : undefined} />
      <col className={highlight === 3 ? classes.highlighted : undefined} />
      <col className={highlight === 4 ? classes.highlighted : undefined} />
      <col className={highlight === 5 ? classes.highlighted : undefined} />
      <col className={highlight === 6 ? classes.highlighted : undefined} />
      <col className={highlight === 7 ? classes.highlighted : undefined} />
      <col />
    </colgroup>
    <TableHead />
    <TableBody data={data} limit={limit} />
  </table>
)

Table.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.string).isRequired,
  highlight: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
}

export default Table
