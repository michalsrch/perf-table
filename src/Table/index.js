import React from 'react'
import { ThemeContext } from '../App/App'
import get from 'lodash/get'

import Table from './Table'
import classes from './Table.less'

const ThemedTable = (props) => (
  <ThemeContext.Consumer>
    {(theme) => <Table {...props} classes={get(theme, 'Table', classes)} />}
  </ThemeContext.Consumer>
)

export default ThemedTable
