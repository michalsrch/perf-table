# Performance benchmark - React + HTML + CSS version

The layout and styling of the table is based on real life use case in PP and in
general using tables is very common use case and the styling and dynamic
behaviour is a standard thing.

Purpose of this is to do a comparison between "web version" and the
reimplementation in react-native-web. The styling and the behaviour should be
exact or closest as possible from user perspective. Also no HTML and CSS escape
hatches should be used since that would defeat the whole point of this
benchmark.


## Installation

Node version used/required - in `.nvmrc` file.

```
npm install
```

Project is using [Parcel](https://parceljs.org/getting_started.html).


## Prod build for tests

```
npm run build
npm run serve
```


## Scenarios to test

* initial page load
* fetch more scenario - rendering new rows (changing limit of items)
* responsive behaviour - changing window size (breakpoint 1025px width)
* highlighting column visually (change highlight value)
* change theme

_Notes:_

* the different scenarios should be tested with different numbers of rows
* memory footprint should be also measured
* quick theming implementation based on similar logic in the sdk was done so it
  can be possible to measure the component visual styling switch


## Development

```
npm start
```


## Disclaimer

The code quality was not the main focus of this repo - things could be improved.
